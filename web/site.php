<?php

/**
 * @Property: gregoriobolivar.com.ve
 * @Author: Gregorio Bolívar
 * @email: elalconxvii@gmail.com
 * @Creation Date: 16/02/2013
 * @Audited by: Gregorio J Bolívar B
 * @Modified Date: 16/02/2013
 * @Description: Code that displays the application's main site.
 * @package: index.php
 * @version: 0.1
 * @Blog: http://gbbolivar.wordpress.com/
 */

// Establish common variables for the system processed by APC 
include_once __DIR__.'/../lib/Core/Autoload.class.php';
autoloader::init();
# mant => Mantenimiento, stable => Desarrollo Estable
new Configuration('Site','mant'); 


?>

