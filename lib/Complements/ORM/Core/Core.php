<?php

require_once (ORM . 'Connection.php');

class Core extends Connection {

    public $conn, $table, $fileTable, $other;
  
    public function Core() {
        $this->conn = new Connection(0);
        $this->cache = new SetVarAll();

        
    }

    /**
     * Metodo de gran importancia dentro de core, debido que se encarga de gestionar si existe el archivo de los querys extras en la carpeta
     * del modelo para asi poder incluirlo en caso contrario poder crearlo poder darle prioridad para su uso.
     */
    public function getTable($name) {
        $this->table = strtolower($name);
        self::findSchema();
        if (self::valid($this->table, $this->cache->getCache('schema'))) {

            $this->fileTable = self::structCame($this->table);
            if (file_exists($file = MODEL . $this->fileTable . 'Table.class.php')) {
                require_once $file;
                $this->other=new $this->fileTable ;
                //var_dump(self::valid($name, $this->cache->getCache('schema')));
                //(self::valid($name, $this->cache->getCache('schema'))) ? require_once $file : die("Error, $name no se encuentra registrada en la base de datos");
            } else {
                //Agregar Structura CameCan
                sprintf('The router application "%s" does not exist.', $file);

                $ar = fopen(MODEL . $this->fileTable . 'Table.class.php', "w+") or die("Problemas en la creaci&oacute;n del router del apps " . $file);
                // Inicio la escritura en el activo
                fputs($ar, '
<?php
/**
 * @propiedad: PROPIETARIO DEL CODIGO
 * @author Gregorio José Bolívar <elalconxvii@gmail.com>
 * @Fecha de Creacion: ' . date('d/m/Y') . '
 * @Auditado por: Gregorio J Bolívar B
 * @Descripción: Generado por el generador de codigo del core.php de webStores
 * @package: datosClass
 * @version: 1.0
 */ 
 class ' . $this->fileTable . ' extends Orm  {
  

 }
 ');

                fputs($ar, " \n");
                fputs($ar, "?>");
                // Cierro el archivo y la escritura
                fclose($ar);
            }
        } else {
            die("Error, $name no se encuentra registrada en la base de datos");
        }
        return $this;
    }

    /**
     * Encargado de gestionar automaticamente la consulta de todos los registros relacionados con la entidad dada.
     * @access public
     * @depends execute
     * @depends num_row
     * @depends all_row
     * @return string Resultado de la consulta general de la entidad dada.
     */
    public function findAll() {

        $sql = "SELECT * from " . $this->table . ';';
        $this->conn->execute($sql);
        $rows = $this->conn->num_row();
        return $this->conn->all_row();
    }

    public function findBy($data = array()) {

        $sql = "SELECT * FROM  categoria_tipo";
        $this->conn->execute($sql);
        $rows = $this->conn->num_row();
        return $this->conn->all_row();
    }

    /**
     * Encargado de leer toda la estructura las tablas con el fin de cargar en cache para su posterior uso
     * @access private
     * @depends readSchema
     * @return string Cache key schema para luego ser utilizado posteriormente
     * @version 1.0
     */
    private function findSchema() {
        $datoJson = array();
        foreach ($this->conn->readSchema() AS $table):
            $datoJson[] = $table['TABLE_NAME'];
        endforeach;
        $this->cache->setCache('schema', $datoJson);
    }

    /**
     * Metodo encargadao de Validar que el nombre de la tabla corresponde con los existente en la estructura existente.
     * @access private
     * @depends findSchema()
     * @param string $table nombre de la tabla que deseamos validar
     * @param array $arraySchema Array de la estructura de todas las tabla
     * @return boolean en caso que exista true de lo contrario false
     * @version 1.0
     */
    private function valid($table, $arraySchema) {
        foreach ($arraySchema as $item):
            if ((string) trim($item) === (string) trim($table)) {
                return TRUE;
            }
        endforeach;
    }

    /**
     * Metodo encargadao de gestionar la estructura UpperCamelCase, cuando la primera letra de cada una de las palabras es mayúscula. Ejemplo: EjemploDeUpperCamelCase.
     * @access private
     * @param string $name El nombre que deseamos parar a UpperCamelCase, debe ser asi la entrada: prueba_tabla
     * @return string UpperCamelCase
     * @version 1.0
     */
    private function structCame($name) {
        $data = explode('_', $name);
        $cant = count($data);
        $came = '';
        switch ((integer) $cant):
            case 1:
                $came = ucwords($name);
                break;
            case $cant > 1:
                foreach ($data AS $item):
                    $came .= ucwords($item);
                endforeach;
                break;
        endswitch;
        return $came;
    }

    public function __destruct() {
        $this->close();
    }

}
