<?php

/**
 * @propiedad: MPPI
 * @Autor: Gregorio Bolivar
 * @email: elalconxvii@gmail.com
 * @Fecha de Creacion: 20/03/2013
 * @Auditado por: Gregorio J Bolivar B
 * @Fecha de Modificacion: 20/03/2013
 * @Descripcin: Complements the system redmercurio
 * @package: Complements.class.php
 * @version: 1.0
 */
class Complements {

     /**
     * Generador de reportes basado en html
     * @param string $html EL html del reporte en pdf
     * @param string $name Nombre del reporte con el cual se guardara el reporte
     */
    static public function setHtmlReporte($html, $name, $barcode = array()) {

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('TCPDF Example 006');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    
    //initialize document
    $pdf->AddPage();
        $pdf->writeHTML($html, true, false, false, false, '');
     if (count($barcode) > 0) {
            // Example of Image from data stream ('PHP rules')
            $imgdata = base64_decode($barcode['img']);
            $pdf->SetXY($barcode['x'], $barcode['y']);
        
            // The '@' character is used to indicate that follows an image data stream and not an image file name
            $pdf->Image('@'.$imgdata, 35, 178, 76);
        }

        //Close and output PDF document
        $pdf->Output($name, $dest = 'F');
        //$pdf->Output($name="reporteDetallado.pdf");

        header('Content-type: application/pdf');

        // Se va a llamar descarga.pdf
        header("Content-Disposition: attachment; filename=$name");

    }

    /**
     * Generador de base64 de archivos 
     * @author Gregorio Bolivar B <elalconxvii@gmail.com>
     * @version 1.0
     * @param string $ruta Ruta del archivo donde se guarda los documentos 
     * @return string Base64 del archivo que se codifico
     * 
     */
    static public function generarBase64($ruta) {
        $data = base64_encode(file_get_contents($ruta));
        return $data;
    }

    /**
     * Eliminar archivo fisico del servidor luego que se genera el string del base 64
     * @author Gregorio Bolivar B <elalconxvii@gmail.com>
     * @version 1.0     * @param string $ruta ruta donde esta creada el archivo en el servidor 
     */
    static public function eliminarArchivoFisico($ruta) {
        unlink($ruta);
    }

}
