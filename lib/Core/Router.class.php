<?php

class Route {

    protected $routes = array();

    function __construct($application, $config) {
        foreach ($config as $route) {
            $this->routes[] = array('objeto' => array('name' => $route->name, 'module' => $route->module, 'method' => $route->method));
        }
        self::getAction($application, $this->routes);
    }

    static public function getAction($application, $option) {
        // Crar un archivo de cache donde tiene las rutas y permitir validar el mismo si existe
        $app = ucfirst($application);
        if (file_exists($file = DIR_SRC . $app . "/Cache/" . $app . 'Router.class.php')) {
            include_once $file;
        } else {
            sprintf('The router application "%s" does not exist.', $app);

            $ar = fopen(DIR_SRC . $app . "/Cache/" . $app . "Router.class.php", "w+") or die("Problemas en la creaci&oacute;n del router del apps " . $application);
            // Inicio la escritura en el activo
            fputs($ar, '
<?php
/**
 * @propiedad: PROPIETARIO DEL CODIGO
 * @Autor: Gregorio Bolivar
 * @email: elalconxvii@gmail.com
 * @Fecha de Creacion: ' . date('d/m/Y') . '
 * @Auditado por: Gregorio J Bolívar B
 * @Descripción: Generado por el generador de codigo de router de webStores
 * @package: datosClass
 * @version: 1.0
 */ 
        
 ');
            // capturador del get que esta pasando por parametro
            fputs($ar, '@$solicitud = explode(\'/\',$_SERVER[\'PATH_INFO\']);');
            fputs($ar, "\n");
            fputs($ar, '$request = @$solicitud[1];');
            fputs($ar, "\n");
            fputs($ar, "\n");

            foreach ($option AS $cont => $routes):
                foreach ($routes AS $route):
                    fputs($ar, '/** Inicio  del Bloque de instancia al proceso de ' . $route['name'] . '  */');
                    fputs($ar, "\n");
                    fputs($ar, '$datos' . $cont . ' = array(\'request\'=>$request, \'name\'=>"' . $route['name'] . '", \'apps\'=>"' . $app . '", \'module\'=>"' . $route['module'] . '",\'method\'=>\'' . $route['method'] . '\');');
                    fputs($ar, "\n");
                    fputs($ar, '$process' . $cont . ' = new RouterGeneratorPrimium($datos' . $cont . ');');
                    fputs($ar, "\n");
                    fputs($ar, '/** Fin del caso de ' . $route['name'] . ' */');
                    fputs($ar, "\n");
                endforeach;
            endforeach;

            fputs($ar, " \n");
            fputs($ar, "?>");
            // Cierro el archivo y la escritura
            fclose($ar);
        }
    }

    public function __destruct() {
        
    }

}

?>
