<?php

class Security{
    public $session;
    public function Security() {
        $this->cache = new SetVarAll();   
        session_start();
        $this->session = null;
   
    }

    public function Autenticado(){
        if(@$_SESSION['autenticado']!='TRUE'){ 
            header('location: '.$this->cache->getCache('urlWebs'));
        }
    }

    public function setSession($key, $value) {
        $this->session=$_SESSION[$key] = $value;
        return $this;
    } 
    // Method encargado de extraer la informacion en cache
    public function getSession($key) {
        if (@$_SESSION[$key]) {
            return $_SESSION[$key];
        } else {
            return false;
            #return ('Session (' . $key . '), no se encuentra registrada.');
        }
        return $this;
    }

    // Method encargado de eliminar la informacion en cache
    public function delSession($key) {
        if ($_SESSION[$key]) {
            unset($_SESSION[$key]);;
        } else {
            return false;        
        }
    }

        // Method encargado de eliminar la informacion en cache
    public function delSessionAll() {
        session_unset();
        session_destroy();
        self::Autenticado();
    }

    public function __destruct() {
        
    }

}

?>
