<?php

/**
 * @Property: sumejornegocio.com
 * @Author: Gregorio Bolívar
 * @email: elalconxvii@gmail.com
 * @Creation Date: 16/02/2013
 * @Audited by: Gregorio J Bolívar B
 * @Modified Date: 23/02/2013
 * @Description: The code that manages the extraction layer.
 * @package: index.php
 * @version: 0.5
 * @Blog: http://gbbolivar.wordpress.com/
 */
//namespace webstore\lib\Core;

class Configuration extends SetVarAll {

    public $cache;

    function Configuration($application, $proceso) {
        $this->SetVarAll();
        $app = ucfirst($application);

        $file = $app . 'Configuration.php';
        $class = $app . 'Configuration';
        $fold = DIR_SRC . $app . '/Config/';


        // Read configuration variables app.ini
        $variable[] = DIR_CONFIG . "app.ini";
        $variable[] = DIR_CONFIG . "view.ini";
        foreach ($variable as $strFileName) {
        
            file_exists($strFileName) ? $objFopen = parse_ini_file($strFileName, true) : die("File not Found " . $strFileName);

            // Load the configuration values for the default block
            foreach ($objFopen['default'] AS $key => $value):
                $this->setCache($key, $value);
            endforeach;

            // Load the configuration values for the load block
            foreach ($objFopen[$app] AS $key => $value):
                $this->setCache($key, $value);
            endforeach;
            # code...
        }

        // Check if there is a configuration file of the application module.
        if (!file_exists($file = DIR_SRC . $app . '/Config/' . $file)) {
            die(sprintf('The application "%s" does not exist.', $app));
        }


        Twig_Autoloader::register();

        switch ($proceso):
            case 'stable':
                require_once $file;
                $loadRouter = new $class;
                $loadRouter->Configure($app, $fold);
                break;

            case 'mant':
                $commun = new CommunController();
                $dir1 = $this->getCache('dir_m_twig');
                $dir2 = $this->getCache('dir_d_twig');
                $this->twig = $commun->communIniTemplate($dir1,$dir2);
                echo $this->twig->render('baseMant.twig', $commun->initSendDataTwig());
                break;
        endswitch;
    }

    public function __destruct() {
        
    }

}

?>
