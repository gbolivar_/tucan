<?php

/**
 * @Property: sumejornegocio.com
 * @Author: Gregorio Bolívar
 * @email: elalconxvii@gmail.com
 * @Creation Date: 23/02/2013
 * @Audited by: Gregorio J Bolívar B
 * @Modified Date: 14/02/2013
 * @Description: generate the code of the main rules for modules.
 * @package: index.php
 * @version: 0.1
 * @Blog: http://gbbolivar.wordpress.com/
 */
class RouterGeneratorPrimium {

    var $get, $url, $mod, $app, $met, $obj, $class, $file;

    public function __construct($datos) {


        $this->get = $datos['request']; // EL VALOR CAPTURADO POR EL METHOD GET
        $this->url = $datos['name'];    // EL VALOR PASADO POR PARAMETRO PARA HACER LA COMPARACION
        $this->mod = $datos['module'];  // LA PAGINA A DONDE SERA REDIRECCIONADA
        $this->app = $datos['apps'];    // NOMBRE DE LA APLICACION
        $this->met = $datos['method'];  // METHOD A INSTANCIAR

        $this->obj = NULL;
        $this->class = NULL;
        $this->file = NULL;
        /**  Caso general para todas las paginas */
        //print_r('url:'.$this->url);
        $p=explode('/',@$_SERVER['PATH_INFO']);
        switch ($this->get) {
            case $this->url :
                $this->class = ucfirst($this->mod) . 'Controller'; 
                $this->file = '../src/' . ucfirst($this->app) . '/Controller/' . ucfirst($this->mod) . 'Controller.php';
                $mthod= $this->met;
                if (!file_exists($this->file)) {
                    die("Problem loading the module action ".ucfirst($this->mod) );                  
                }

                include_once $this->file;
                $obj =& new $this->class;
                $obj->$mthod($_REQUEST);
                break;
        }
    }

    public function __destruct() {
        
    }

}

?>
