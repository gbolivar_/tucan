<?php

/**
 * @Property: Clixcreen.com
 * @Author: Clixcreen.com
 * @email: webmaster@clixcreen.com
 * @Creation Date: 09/01/2014
 * @Audited by: Gregorio J Bolívar B
 * @Modified Date: 09/01/2014
 * @Description: Code that displays the application's main site.
 * @package: HomeController.php
 * @version: 0.1
 */

class CommunController {

	public function communIniTemplate($d1,$d2) {
		$this->load = new Twig_Loader_Filesystem(array($d1, $d2));
		$this->twig = new Twig_Environment($this->load, array('debug' => true));
		return $this->twig ;
	}
	public function dennyController(){
		header('Access-Control-Allow-Origin: http://localhost');
		header("Access-Control-Allow-Credentials: true");
		header('Access-Control-Allow-Headers: X-Requested-With');
		header('Access-Control-Allow-Headers: Content-Type');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 86400');
	}
	public function cifrarHTML($html){
		$buffer=$html;		
		$search = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
		$replace = array('>','<','\\1');
		$pre = preg_replace($search, $replace, $buffer);
		return base64_encode($pre);
	}
	/**
	 *  Method encargado de gestionar con la api restfull
	 *  @dataBasic Encargada de tener la configuracion donde se encuentra el api rest + usuario y clave
	 *  @data Encargada de contener un arreglo de los datos que seran enviado al api rest
	 */
	public function clientRestBase($dataBasic, $data){
		/*  @$dataBasic
		    [apRest] => http://192.168.0.103:8084/rumberos/register
		    [tocken] => 123456
		    [secret] => 123456
		    [method] => POST
		*/

		    $dataJson = '';
		    // Validar si el ApiRest esta activo
		    $connec = self::validateRowsForm('URLACT',$dataBasic['apRest']);
		    if(!$connec){
		    	$dataJson['error'] = 1;
		    	$dataJson['message'] = 'Error, Unable to Connect establer ApiRest the Clixcreen.';
		    	return json_encode($dataJson); 
		    }
		    $handle = curl_init();



		    curl_setopt($handle, CURLOPT_URL, $dataBasic['apRest']);
		    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($handle, CURLOPT_USERPWD, $dataBasic['tocken'].":".$dataBasic['secret']);
		    curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
		    curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);

		    switch($dataBasic['method'])
		    {

		    	case 'GET':
		    	break;

		    	case 'POST':
		    	curl_setopt($handle, CURLOPT_POST, true);
		    	curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($data));
		    	break;

		    	case 'PUT':
		    	curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'PUT');
		    	curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($data));
		    	break;

		    	case 'DELETE':
		    	curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'DELETE');
		    	break;
		    }

		    $response = curl_exec($handle);
		    curl_close($handle);

		    return $response;
		}

		public function validateRowsForm($type, $data){
			switch($type)
			{
		    	case 'REQ': // Dato requ
		    	$retorno=($data == '')? FALSE: TRUE;
		    	break;

		    	case 'NUM': // Solo númericos
		    	$retorno=(filter_var($data, FILTER_VALIDATE_INT) === FALSE)? FALSE: TRUE;
		    	break;

		    	case 'LET': // Solo letras
		    	$retorno=(filter_var($data, FILTER_VALIDATE_INT) === FALSE)? FALSE: TRUE;

		    	break;

		    	case 'STR': // Solo String alfanumerico
		    	$retorno=(filter_var($data, FILTER_SANITIZE_STRING) === FALSE)? FALSE: TRUE;

		    	break;

		    	case 'EMA': // Solo correos electrinico
		    	$retorno=(filter_var($data, FILTER_VALIDATE_EMAIL) === FALSE)? FALSE: TRUE;
		    	break;

		    	case 'URL': // Solo direcciones de internet
		    	$retorno=(filter_var($data, FILTER_VALIDATE_URL,FILTER_FLAG_QUERY_REQUIRED) === FALSE)? FALSE: TRUE;
		    	break;

		    	case 'BOO': // Identificar si el registro es booleano
		    	$retorno=(filter_var($data, FILTER_VALIDATE_BOOLEAN) === FALSE)? FALSE: TRUE;
		    	break;
		    	case 'URLACT':
		    	$retorno=(@get_headers($data))? TRUE : FALSE;
		    	break;
		    }
		    return $retorno;
		}

		public function file_get_contents_curl($url) 
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

			$data = curl_exec($ch);
			curl_close($ch);

			return $data;

		}

		public function get_meta_tags_curl($url){
			$html = self::file_get_contents_curl($url);

			//parsing begins here:
			$doc = new DOMDocument();
			@$doc->loadHTML($html);
			$nodes = $doc->getElementsByTagName('title');

			//get and display what you need:
			$data["title"] = $nodes->item(0)->nodeValue;

			$metas = $doc->getElementsByTagName('meta');
			for ($i = 0; $i < $metas->length; $i++)
			{
			$meta = $metas->item($i);
			if($meta->getAttribute('name') == 'description')
			    $data["meta"]["description"] = $meta->getAttribute('content');
			if($meta->getAttribute('name') == 'keywords')
			    $data["meta"]["keywords"] = $meta->getAttribute('content');
			}
			return $data;

		}

		public function urlfacebook(){
			$state = md5(uniqid(rand(), TRUE));  
			$_SESSION['state'] = $state;
			$redirect_uri  = "http://clixcreen.com/develop/web/index.php/facebook_authentication" ;
			$url = 'https://www.facebook.com/dialog/oauth?client_id=363266147147853&redirect_uri=' .    $redirect_uri  . '&state='.  $state . '&scope=email';
			return $url;
		}

		private function initMailer() {
			/** Instancio la class de envio de correo */
			$this->mail = new PHPMailer();
			/** Verifico si la variables del app se pueden instanciar dependiendo cuando es por el cron se cargan las definidas */
            $this->mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
            $this->mail->CharSet = 'UTF-8';
            $this->mail->SMTPAuth = true;                  // enable SMTP authentication
            $this->mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
            $this->mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
            $this->mail->Port = 465;                   // set the SMTP port for the GMAIL server
            $this->mail->Username = "proyecto.mppi@gmail.com";  // GMAIL username
            $this->mail->Password = "#industria#";            // GMAIL password
            $this->mail->SetFrom("proyecto.mppi@gmail.com", "Info Clixcreen, Inc.");
            $this->mail->IsSMTP();
        }

        private function sendMailer() {
        $this->mail->IsHTML(true); // send as HTML
        if (!$this->mail->Send()) {
            return false; //Error: " . $this->mail->ErrorInfo;
        } else {
        	return true;
        }
    }

    public function mailNotification($correos, $html, $other) {
        // Inicializar el proceso para el envio de mensaje de correo
    	self::initMailer();

    	/** Procedimiento para notificar via correo a los responsables */
    	foreach ($correos as $correo):
    		$this->mail->Subject = $other['subject'];
    	$this->mail->AltBody = $other['body'];
            $this->mail->WordWrap = 50; // set word wrap
            $this->mail->MsgHTML($html);   // Cuerpo del mensaje
           /*if ($notificacion['adjunto'] != '') {
                $this->mail->AddAttachment(self::rutaFile() . '' . $notificacion['adjunto'] . '');   //Documento Adjunto
            }*/

            $this->mail->AddAddress($correo['email'], $correo['firstname'] . ' ' . $correo['surname']);
            endforeach;

            self::sendMailer();
        }
    /**
		Permite mejorar los enlaces nativos de php a url amigables
		(String) $url ejemplo: http://ruta/index.php?id=prueba de sistema
    */
		public function urls_friend($url) {
			$url = strtolower($url);
			$find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
			$repl = array('a', 'e', 'i', 'o', 'u', 'n');
			$url = str_replace ($find, $repl, $url);
			$find = array(' ', '&', '\r\n', '\n', '+');
			$url = str_replace ($find, '-', $url);
			$find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
			$repl = array('', '-', '');
			$url = preg_replace ($find, $repl, $url);
			return $url;
		}

		// Cambiar el _id del mongo a un string return.
		public function initSendDataTwig(){
			$this->cache = new SetVarAll();
			$dataSendTwig['namSite'] =  $this->cache->getCache('namSite');
	        $dataSendTwig['home'] =  $this->cache->getCache('urlComp');
	        $dataSendTwig['url_web'] = $this->cache->getCache('urlWebs');
	        $dataSendTwig['csss'] = explode(',',$this->cache->getCache('css'));
	        $dataSendTwig['jsss'] = explode(',',$this->cache->getCache('js'));
	        $dataSendTwig['srcImg'] = $this->cache->getCache('srcImg');
	        $dataSendTwig['srcCss'] = $this->cache->getCache('srcCss');
	        $dataSendTwig['srcJs'] = $this->cache->getCache('srcJs');
	        $dataSendTwig['timesPMant'] = $this->cache->getCache('timesPMant');
	        $dataSendTwig['titlePMant'] = $this->cache->getCache('titlePMant');
	        $dataSendTwig['mensgPMant'] = $this->cache->getCache('mensgPMant');

	        return $dataSendTwig;
		}
	}

	?>
