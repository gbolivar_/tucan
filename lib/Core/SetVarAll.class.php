<?php

require_once(CACHELITE.'Cache.php');
//namespace lib\Complements\CacheLite;
class SetVarAll {

    var $cache;

    public function SetVarAll() {
        $this->cache = new Cache('file', array('cache_dir' => __DIR__.'/../../src/Site/Cache/')); //../app/cache/
        //$this->cache = new Cache('file', array('cache_dir' => '/tmp/')); //../app/cache/
    }

    // Method encargado de guardar la informacion en cache
    public function setCache($key, $value) {
        $this->cache->save($key, $value);
        return $this;
    }

    // Method encargado de extraer la informacion en cache
    public function getCache($key) {
        if ($this->cache->isCached($key)) {
            return $this->cache->get($key);
        } else {
            die('Variable (' . $key . '), no se encuentra registrada.');
        }
        return $this;
    }

    // Method encargado de eliminar la informacion en cache
    public function rmCache($key) {
        if ($this->cache->isCached($key)) {
            $this->cache->remove($key);
        } else {
            die('!Error, Variable (' . $key . '), no se encuentra registrada. ');
        }
    }

    public function __destruct() {
        
    }

}

?>
