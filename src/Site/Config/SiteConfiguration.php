<?php
class SiteConfiguration {
    public function Configure($application,$folder) {
        $config_file = $folder.'Router.xml';
        $config = simplexml_load_file($config_file);
        new Route($application,$config);
    }
    
    public function __destruct() {   
    }
}
?>
