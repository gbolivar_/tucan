<?php

/**
 * @Property: gregoriobolivar.com.ve
 * @Author: Gregorio Bolívar
 * @email: elalconxvii@gmail.com
 * @Creation Date: 02/11/2015
 * @Audited by: Gregorio J Bolívar B
 * @Modified Date: 02/11/2015
 * @Description: Code that displays the application's main site.
 * @package: action.class.php
 * @version: 0.1
 * @webs: http://www.gregoriobolivar.com.ve/ 
 * @Blog: http://gbbolivar.wordpress.com/
 */


class HomeController {

    public $security, $cache, $commun, $table, $dir1, $dir2, $twig;

    public function __construct() {
        $this->security = new Security();
        $this->cache = new SetVarAll();
        $this->commun = new CommunController();
        $this->table = new Orm();

        $this->commun->dennyController();

        $this->dir1 = $this->cache->getCache('dir_d_twig');
        $this->dir2 = $this->cache->getCache('dir_s_twig');

        $this->twig = $this->commun->communIniTemplate($this->dir1,$this->dir2);


        $this->dataBasic = array(
            'tocken' => $this->cache->getCache('apiTock'),
            'secret' => $this->cache->getCache('apiSecr'),
            'method' => 'GET'
        );

    }

    public function runIndex($request) {

         // Se hace manda el registro en la base de datos mongo 
        $dataPost = array();
        $dataBasic = array_merge(array('apRest' => $this->cache->getCache('apiRest').'metas/getAll'), $this->dataBasic);
        $base= $this->commun->ClientRestBase($dataBasic, $dataPost); 

        $a = json_decode($base);
        
 
        $dataSendTwig = array_merge(array('metas' => $a->data), $this->commun->initSendDataTwig());

        echo $this->twig->render('forntend.twig', $dataSendTwig);
    }



}

?>
